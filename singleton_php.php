<?php
//ConfigManager class that manages application configuration settings:
class ConfigManager
{
    private static $instance;
    private $configFile;
    private function __construct()
    {
        $this->configFile = "config.properties";
        // Load configuration settings from file or other sources
    }
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }
    public function getConfig($key)
    {
        // Retrieve configuration setting based on the key
        return "Value for $key";
    }
}
$configManager = ConfigManager::getInstance();
$value1 = $configManager->getConfig("setting1");
$value2 = $configManager->getConfig("setting2");
echo "Setting 1: $value1\n";
echo "Setting 2: $value2\n";
// Since it's a Singleton, you get the same instance no matter how many times you call getInstance()
$configManagerAgain = ConfigManager::getInstance();

var_dump($configManager === $configManagerAgain); // Output: bool(true) - Both instances are the same